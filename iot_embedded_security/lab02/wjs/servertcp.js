var net = require('net');
var crypto = require('crypto');
var zlib = require('zlib');
var Stream = require('stream')
var str = require('string-to-stream')
var password = new Buffer('parola123');
var decryptStream = crypto.createDecipher('aes-256-cbc', password);


var server = net.createServer(
    function (sock) {
        var gzip = zlib.createGzip();
        console.log('a client arrived to the server')
        // var rb
        // sock.on('data', function (recvData) {
        //     console.log('Client sent 2 server -> ' + recvData);

        //     
            
        //     str(recvData)
        //     .pipe(gzip)  // uncompresses
        //     .pipe(decryptStream) // decrypts
        //     .pipe(process.stdout)  // writes to terminal
        //     .on('finish', () => {  // finished
        //         console.log('done');
        //     });
        // });

        sock
        .pipe(gzip)
        .pipe(decryptStream)
        .pipe(process.stdout)
        .on('finish', () => {
            console.log('done')
        })

        sock.on('connect', function() {
           
        });

        sock.on('end', function () {
            console.log('Client disconnected');
            

        });

        sock.on('close', function (data) {
            console.log('CLOSED => ' + sock.remoteAddress + ':' + sock.remotePort);
        });

        sock.write('Hello from the server\r\n')
    }
);

server.listen(8090, function() {
    console.log('server is listening on port 8090')
});