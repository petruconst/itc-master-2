var PORT = 33333;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var message = Buffer.from('udp4 message from the client');

var client = dgram.createSocket('udp4');

client.send(message, 0, message.length, PORT, HOST, function (err, bytes) {
    if (err != null)
        throw err;
    
    console.log('UDP message client to server = ' + HOST + ':' + PORT);
    client.close()
});