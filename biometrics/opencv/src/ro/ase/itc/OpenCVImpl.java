package ro.ase.itc;

public class OpenCVImpl {
    public void run() {

        CascadeClassifier faceDetector = new CascadeClassifier(getClass().getResource("resources//lbpcascade_frontalface.xml").getPath());
        Mat image = Highui.imread(getClass().getResource("resources/faces.png").getPath());

        // Detect faces using the classifier initialized above
        MatOfRect faces = new MatOfRect();
        faceDetector.detectMultiScale(image, faces);
        System.out.println("No of faces detected: " + faces.toArray().length);

        for (Rect rect: faces.toArray())
        {
            core.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(255, 0, 0));
        }

        String fileName = "detectedFaces.png";

        Highhui.imgwrite(fileName, image);
    }
}
