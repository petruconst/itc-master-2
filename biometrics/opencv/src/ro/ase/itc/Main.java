package ro.ase.itc;

import org.opencv.core.Core;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Hello from OpenCV!");
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        new OpenCVImpl().run();
    }
}
