#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

void detectAndDisplay(Mat);
cv::String face_cascade_name = "resources/haarcascade_frontalface_alt.xml";
cv::String eye_cascade_name  = "resources/haarcascade_eye_eyeglasses.xml";
cv::String input_file_name = "resources/faces.jpg";
CascadeClassifier face_cascade;
CascadeClassifier eye_cascade;
cv::String window_name = "Capture - face detection";

RNG rng(12345);

int main(int argc, char** argv )
{
    CvCapture* capture;
    Mat frame;

    if (!face_cascade.load(face_cascade_name))
    {
        std::cout << "Error loading classifier";
        return 1;
    }

    if (!eye_cascade.load(eye_cascade_name))
    {
        std::cout << "Error loading classifier";
        return 1;
    }

    capture = cvCaptureFromCAM(0);
    if (capture)
    {
        while (true)
        {
            frame = cvQueryFrame(capture);
            if (!frame.empty())
            {
                detectAndDisplay(frame);
                // break;
            }
            else
            {
                std::cout << "No frame captured";
                break;
            }

            int c = waitKey(63);
            if (static_cast<char>(c) == 'c')
            {
                std::cout << "STOP";
                break;
            }
        }
    }

    // frame = imread(input_file_name, CV_LOAD_IMAGE_COLOR);
    // detectAndDisplay(frame);
    return 0;
}

void detectAndDisplay(Mat frame)
{
    std::vector<Rect> faces;
    Mat frame_gray;

    cvtColor(frame, frame_gray, CV_BGR2GRAY);

    //detect face
    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));
    for (auto& it : faces)
    {
        Point center(
            it.x + it.width * 0.5,
            it.y + it.height * 0.5
        );
        ellipse(frame, center, Size( it.x + it.width * 0.5, it.y + it.height * 0.5), 0, 0, 360, Scalar(255, 0, 0), 4, 8, 0);
        Mat faceROI = frame_gray(it);
        std::vector<Rect> eyes;
        eye_cascade.detectMultiScale(faceROI, eyes, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));

    }
    imshow(window_name, frame);
    waitKey(0);
}