package ro.ism.ase;

public class FXRate {
    private String uid;

    private int id;
    private String data;
    private String euro;
    private String dolar;
    private String gbp;
    private String aur;

    public FXRate() {}

    public FXRate(int id, String data, String euro, String dolar, String gbp, String aur) {
        this.uid = uid;
        this.id = id;
        this.data = data;
        this.euro = euro;
        this.dolar = dolar;
        this.gbp = gbp;
        this.aur = aur;
    }

    @Override
    public String toString() {
        return id + " " + data + " " + euro + " " + dolar + " " + gbp + " " + aur;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getEuro() {
        return euro;
    }

    public void setEuro(String euro) {
        this.euro = euro;
    }

    public String getDolar() {
        return dolar;
    }

    public void setDolar(String dolar) {
        this.dolar = dolar;
    }

    public String getGbp() {
        return gbp;
    }

    public void setGbp(String gbo) {
        this.gbp = gbo;
    }

    public String getAur() {
        return aur;
    }

    public void setAur(String aur) {
        this.aur = aur;
    }
}
