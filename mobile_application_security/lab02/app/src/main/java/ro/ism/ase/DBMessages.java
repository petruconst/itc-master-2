package ro.ism.ase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBMessages
{
    AccesBD accesBD;

    protected static final String COL_ID = "_id";
    protected static final String COL_PLAIN_TEXT = "plainText";
    protected static final String COL_CIPHER_TEXT = "cipherText";
    protected static final String COL_ENC_KEY = "encryptionKey";
    protected static final String COL_PHONE = "phoneNumber";

    DBMessages(Context context)
    {
        accesBD = new AccesBD(context);
    }

    void close()
    {
        accesBD.close();
    }

    long addRecord(Message messNew)
    {
        SQLiteDatabase bd = null;
        long rezInsert = 0;
        ContentValues valori = new ContentValues();

        try
        {
            bd = accesBD.getWritableDatabase();

            valori.put(COL_PLAIN_TEXT, messNew.getPlainText());
            valori.put(COL_CIPHER_TEXT, messNew.getCipherText());
            valori.put(COL_ENC_KEY, messNew.getEncryptionKey());
            valori.put(COL_PHONE, messNew.getPhoneNumber());

            rezInsert = bd.insert(AccesBD.MESSAGES,
                    null, valori);

            bd.close();
        }
        catch(SQLException ex)
        {
            Log.e("TABLE_MESSAGES", ex.getMessage());
        }

        return rezInsert;
    }

    int deleteRecord(String where, String[] paramWhere)
    {
        SQLiteDatabase bd = null;
        int rezDelete = 0;
        try
        {
            bd = accesBD.getWritableDatabase();
            rezDelete = bd.delete(AccesBD.MESSAGES, where, paramWhere);

            bd.close();
        }
        catch(SQLException ex)
        {
            Log.e("TABLE_MESSAGES", ex.getMessage());
        }

        return rezDelete;
    }

    Cursor obtineInregistrariCursor(String[] coloane,
                                    String cond, String[] paramCond)
    {
        SQLiteDatabase bd = null;

        Cursor rez = null;

        try
        {
            bd = accesBD.getWritableDatabase();
            rez = bd.query(AccesBD.MESSAGES, coloane,
                    cond, paramCond, null, null,
                    COL_PHONE + " ASC");
        }
        catch(SQLException ex)
        {
            Log.e("TABLE_MESSAGES", ex.getMessage());
        }

        return rez;
    }

    Message [] obtineInregistrari(String[] coloane,
                                  String cond, String[] paramCond)
    {
        SQLiteDatabase bd = null;

        Message [] curs = null;

        try
        {
            bd = accesBD.getWritableDatabase();
            Cursor rez = bd.query(AccesBD.MESSAGES,
                    coloane, cond, paramCond, null, null,
                    COL_PHONE + " ASC");
            int nInreg = rez.getCount();
            curs = new Message[nInreg];

            rez.moveToFirst();
            for (int i=0; i < nInreg; i++)
            {
                curs[i] = new Message(rez.getString(1),
                        rez.getString(2), rez.getString(3),
                        rez.getString(4));
                curs[i].setId(rez.getInt(0));
                rez.moveToNext();
            }

            rez.close();
            bd.close();
        }
        catch(SQLException ex)
        {
            Log.e("TABLE_MESSAGES", ex.getMessage());
        }
        return curs;
    }
}