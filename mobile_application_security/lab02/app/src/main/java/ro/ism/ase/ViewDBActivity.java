package ro.ism.ase;


import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ViewDBActivity extends AppCompatActivity {

    DBMessages bd;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        HorizontalScrollView sv = new
                HorizontalScrollView(this);

        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(LinearLayout.VERTICAL);

        //afisare cursuri din BD
        bd = new DBMessages(this);
        String[] coloane = new String[]{DBMessages.COL_ID,
                DBMessages.COL_PLAIN_TEXT, DBMessages.COL_CIPHER_TEXT,
                DBMessages.COL_ENC_KEY, DBMessages.COL_PHONE};

        Cursor rez = bd.obtineInregistrariCursor(coloane,
                null, null);

        ListView lv = new ListView(this);
        //initializare adaptor
        SimpleCursorAdapter arr = new
                SimpleCursorAdapter(this, R.layout.elem, rez,
                coloane, new int[] {R.id.col_id,
                R.id.col_pt, R.id.col_ct,
                R.id.col_ek, R.id.col_pn});
        //asociere adaptor
        lv.setAdapter(arr);

        //trebuie inchisa separat
        bd.close();

        TextView tv1 = new TextView(this);
        tv1.setText("List of messages from the database:"+"\n");

        TextView tv2 = new TextView(this);
        tv2.setText("ID     PlainText     CipherText     EncryptionKey     PhoneNumber"+ "\n");

        ll.addView(tv1);
        ll.addView(tv2);
        ll.addView(lv);
        sv.addView(ll);
        setContentView(sv);
    }
}