package ro.ism.ase;

import android.content.Context;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AccesBD extends SQLiteOpenHelper {

    public static String MESSAGES = "Messages";

    protected static String BAZA_DE_DATE = "Messages.db";

    public static String CREATE_TABLE_MESSAGES =
            "CREATE TABLE " + MESSAGES +"  " +
                    "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "plainText TEXT, cipherText TEXT, encryptionKey TEXT, " +
                    "phoneNumber TEXT)";

    public AccesBD(Context context)
    {
        super(context, BAZA_DE_DATE, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try
        {
            sqLiteDatabase.execSQL(CREATE_TABLE_MESSAGES);
        }
        catch(SQLException ex)
        {
            Log.e("TABLE_MESSAGES", ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + MESSAGES);
        onCreate(sqLiteDatabase);
    }
}