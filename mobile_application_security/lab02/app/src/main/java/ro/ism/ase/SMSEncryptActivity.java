package ro.ism.ase;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class SMSEncryptActivity extends AppCompatActivity {
    public static final int DATABASE = 0;
    public static final int SHOWDB = 1;

    private EditText textToEncrypt;
    private EditText phoneNo;
    private Spinner spinner;
    private Button btnEncrypt;
    private Button btnSend;

    String phoneNumber = "000000000";
    BigInteger plaintext;
    BigInteger ciphertext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsencrypt);

        textToEncrypt = (EditText)findViewById(R.id.textToEncrypt);
        phoneNo = (EditText)findViewById(R.id.phoneNumber);
        spinner = (Spinner)findViewById(R.id.spinner);
        btnEncrypt = (Button)findViewById(R.id.encrypt);
        btnSend = (Button)findViewById(R.id.send);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneNumber = phoneNo.getText().toString();
                if (phoneNumber.equals(""))
                {
                    String[] arr = spinner.getSelectedItem().toString().split(" ");
                    phoneNumber = arr[arr.length - 1];
                    phoneNo.setText(phoneNumber);
                }

                RSA rsa = new RSA(1024);
                String text1 = textToEncrypt.getText().toString();
                plaintext = new BigInteger(text1.getBytes());
                ciphertext = rsa.encrypt(plaintext);
                Toast.makeText(getBaseContext(), "Ciphertext: " + ciphertext, Toast.LENGTH_SHORT).show();

                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(phoneNumber, null, ciphertext.toString(), null, null);
                    Toast.makeText(getApplicationContext(), "Sms sent!", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                }
            }
        });

        List<String> listOfContacts = new ArrayList<String>();
        Contact c = new Contact("1", "Cristi", "072421421");
        listOfContacts.add(c.toString());

        List<String> listFiltered = new ArrayList<String>();
        for (String s : listOfContacts) {
            String[] elem = s.split(" ");
            if (elem[elem.length - 1].matches("[0-9]")) {
                listFiltered.add(s);
            }
            listFiltered.add(s);
        }

        String[] stringArray = listFiltered.toArray(new String[listFiltered.size()]);
        ArrayAdapter<String> adaptor = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, stringArray);
        spinner.setAdapter(adaptor);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, DATABASE, 0, "Save message in database");
        menu.add(0, SHOWDB, 1, "SHOW DATABASE");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case DATABASE:
                DBMessages db = new DBMessages(this);
                try {
                    BigInteger key = new BigInteger(ciphertext.toString());
                    Message mess = new Message(plaintext.toString(), ciphertext.toString(), key.toString(), phoneNumber);
                    db.addRecord(mess);
                    Toast.makeText(getApplicationContext(), "Message saved in the database", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case SHOWDB:
                Intent intent = new Intent(this, ViewDBActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }
}
;