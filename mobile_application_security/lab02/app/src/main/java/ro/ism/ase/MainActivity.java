package ro.ism.ase;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    public static final int CONVERTOR = 0;
    public static final int SMSENCRYPT = 1;
    public static final int NETWORK = 2;
    public static final int JSON = 3;
    public static final int EXIT = 4;

    TextView tvDate = null;
    EditText EUR = null;
    EditText USD = null;
    EditText GBP = null;
    EditText XAU = null;

    public enum Location {
        INTERNAL, DAATABASE, SDCARD;

        public static Location getLocation(int ordinal) {
            return Location.values()[ordinal];
        }

        public static String[] getValues() {
            String[] values = new String[Location.values().length];
            int i = 0;
            for (Location loc : Location.values()) {
                values[i++] = loc.toString();
            }
            return values;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvDate = findViewById(R.id.date);
        EUR = findViewById(R.id.editTextEUR);
        USD = findViewById(R.id.editTextUSD);
        GBP = findViewById(R.id.editTextGBP);
        XAU = findViewById(R.id.editTextXAU);

        final Spinner spinnerLocation = findViewById(R.id.spinnerSave);
        spinnerLocation.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, Location.getValues()));


        Log.e("lifecycle", "call of onCreate");

        Button btn1 = findViewById(R.id.fx_btn);
        btn1.setOnClickListener(this);

        Button btn2 = findViewById(R.id.save_btn);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(), "Click on 1st button", Toast.LENGTH_LONG);
//                Intent intent = new Intent(getApplicationContext(), ConertorActivity.class);
//                startActivity(intent);
                int id = spinnerLocation.getSelectedItemPosition();
                FXRate cv = new FXRate(id, tvDate.getText().toString(), EUR.getText().toString(), USD.getText().toString(), GBP.getText().toString(), XAU.getText().toString());
                Log.e("fx", cv.toString());
                try {
                    writeCVToFile("file.dat", cv);
                    cv = null;
                    cv = readCVFromFile("file.dat");
                    Toast.makeText(getApplicationContext(), cv.toString(), Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("lifecycle", "call of onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("lifecycle", "call of onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("lifecycle", "call of onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("lifecycle", "call of onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("lifecycle", "call of onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("lifecycle", "call of onDestroy");
    }


    @Override
    public void onClick(View v) {
//        Toast.makeText(this, "Click on 1st button", Toast.LENGTH_LONG);
//        Intent intent = new Intent(this, SMSEncryptActivity.class);
//        startActivity(intent)

        Network  p = new Network() {
            @Override
            protected void onPostExecute(InputStream inputStream) {
                tvDate.setText(cv.getData());
                EUR.setText(cv.getEuro());
                USD.setText(cv.getDolar());
                GBP.setText(cv.getGbp());
                XAU.setText(cv.getAur());
            }
        };

        try {
            p.execute(new URL("https://www.bnr.ro/nbrfxrates.xml"));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, CONVERTOR, 0, "FX Convertor");
        menu.add(0, SMSENCRYPT, 1, "SMS Encrypt");
        menu.add(0, NETWORK, 2, "Network");
        menu.add(0, JSON, 3, "JSON");
        menu.add(0, EXIT, 4, "Eit");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case CONVERTOR:
                Intent intent3 = new Intent(getApplicationContext(), ConertorActivity.class);
                startActivity(intent3);
                break;
            case SMSENCRYPT:
                Intent intent2 = new Intent(this, SMSEncryptActivity.class);
                startActivity(intent2);
                break;
            case NETWORK:
                Network n = new Network();
                String FXRates = n.ExtractFXRates();
                String message;
                if (FXRates != null) {
                    message =  FXRates;
                } else {
                    message = "Internet connection error";
                }
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                break;
            case JSON:
                Intent intent4 = new Intent(this, JSONActivity.class);
                startActivity(intent4);
                break;
            case EXIT:
                android.os.Process.killProcess(android.os.Process.myPid());
                return true;
        }
        return false;
    }

    private void writeCVToFile(String fileName, FXRate cv) throws IOException {
        FileOutputStream file = openFileOutput(fileName, Activity.MODE_PRIVATE);
        DataOutputStream dos = new DataOutputStream(file);

        dos.writeInt(cv.getId());
        dos.writeUTF(cv.getData());
        dos.writeUTF(cv.getEuro());
        dos.writeUTF(cv.getDolar());
        dos.writeUTF(cv.getGbp());
        dos.writeUTF(cv.getAur());
        dos.flush();
        file.close();
    }

    private FXRate readCVFromFile(String filename) throws IOException {
        FileInputStream file = openFileInput(filename);
        DataInputStream dis = new DataInputStream(file);
        int id = dis.readInt();
        String data = dis.readUTF();
        String euro = dis.readUTF();
        String dolar = dis.readUTF();
        String gbp = dis.readUTF();
        String aur = dis.readUTF();
        FXRate cv = new FXRate(id, data, euro, dolar, gbp, aur);

        file.close();
        return cv;
    }
}
