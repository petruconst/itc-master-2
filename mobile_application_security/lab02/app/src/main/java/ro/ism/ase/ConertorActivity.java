package ro.ism.ase;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.HashMap;

public class ConertorActivity extends AppCompatActivity {

    public EditText valuta1;
    public EditText valuta2;
    public Spinner sp1;
    public Spinner sp2;
    public Dialog diag;

    String[] options = {"EUR", "USD", "GBP", "XAU", "RON"};
    public static HashMap<String, Double> RATES;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        RATES = new HashMap<String, Double>();
        RATES.put("EUR", 4.9);
        RATES.put("USD", 4.3);
        RATES.put("XAU", 160.2);
        RATES.put("RON", 1.0);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conertor);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, options);

        LinearLayout ll = findViewById(R.id.convertor);

        TextView tvTitle = new TextView(this);
        tvTitle.setText("FX Convertor:");
        tvTitle.setGravity(Gravity.CENTER);

        valuta1 = new EditText(this);
        valuta1.setTextSize(15);
        sp1 = new Spinner(this);
        sp1.setAdapter(adapter);

        valuta2 = new EditText(this);
        valuta2.setTextSize(15);
        sp2 = new Spinner(this);
        sp2.setAdapter(adapter);

        Button btn = new Button(this);
        btn.setText("Convert");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment2 textFragment = new Fragment2();
                textFragment.changeTextSize(50);

                double val1 = 0.0;
                try {
                    val1 = Double.parseDouble(valuta1.getText().toString());





                } catch (Exception e) {
                    Log.e("err", "error");
                }
                String base = sp1.toString();
                String term = sp2.toString();
                Log.i("dbg", term);
                Log.i("dbg", base);
//                double rate1 = RATES.get(base);
//                double rate2 = RATES.get(term);
                double val2 = val1 * 4.7;
                valuta2.setText(Double.toString(val2));

                diag.show();
            }
        });

        ll.addView(tvTitle);
        ll.addView(valuta1);
        ll.addView(sp1);
        ll.addView(valuta2);
        ll.addView(sp2);
        ll.addView(btn);

        diag = new Dialog(this);
//        LinearLayout lld = new LinearLayout(this);
//        btn = new Button(getApplicationContext());
//        btn.setText("Close dialog");
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            }
//        });

        diag.setTitle("Dialog");
//        lld.addView(btn);
        diag.setContentView(R.layout.dialog);
    }

    public void onClick1(View view) {
        diag.dismiss();
    }
}
