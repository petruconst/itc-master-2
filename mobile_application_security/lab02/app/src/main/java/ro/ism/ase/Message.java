package ro.ism.ase;

public class Message {
    private int id;
    private String plainText;
    private String cipherText;
    private String encryptionKey;
    private String phoneNumber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public String getCipherText() {
        return cipherText;
    }

    public void setCipherText(String cipherText) {
        this.cipherText = cipherText;
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Message(String pt, String ct, String ek, String pn) {
        super();
        this.plainText = pt;
        this.cipherText = ct;
        this.encryptionKey = ek;
        this.phoneNumber = pn;
    }
}
