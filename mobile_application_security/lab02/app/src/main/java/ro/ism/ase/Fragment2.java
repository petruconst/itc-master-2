package ro.ism.ase;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {
private static TextView textView;

    public Fragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_fragment2, container, false);

        View fragmentView = inflater.inflate(R.layout.fragment_fragment2, container, false);
        textView = fragmentView.findViewById(R.id.tvFragment2);

        return fragmentView;
    }

    public void changeTextSize(float textSize) {
        if (textView != null)
        {
            textView.setTextSize(textSize);
            textView.setText("ANDROID ISM");
        }
    }

}
