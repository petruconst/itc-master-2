class MyClass {
    private var name: String = "Datafield name"

    fun printMe() {
        println("MyClass::name = " + this.name + " this = " + this)
    }
}

class MyClassWPrimaryConstr(nameStr: String? = null) {
    private var name: String? = nameStr
    init {
        println("init MyClassWPrimaryConstr")
    }

    fun printMe() {
        println("MyClassWPrimaryConstr::name = " + this.name + " this = " + this)
    }
}

class MyTime {
    val hours: Int
    val min: Int
    val sec: Int

    constructor() {
        this.hours = 0
        this.min = 0
        this.sec = 0
    }

    constructor(h: Int, m: Int, s: Int) {
        this.hours = h
        this.min = m
        this.sec = s
    }

    constructor(obj: MyTime) {
        this.hours = obj.hours
        this.min = obj.min
        this.sec = obj.sec
    }

    constructor(sStr: String) {
        val subs = sStr.split(":")
        this.hours = subs[0].toInt()
        this.min = subs[1].toInt()
        this.sec = subs[2].toInt()
    }

    fun serialize(): String {
        return "{%02d}{%02d}{%02d} - this = %s".format(this.hours, this.min, this.sec, this)
    }
}

fun main(args: Array<String>) {
    val obj = MyClass()
    obj.printMe()

    val o1 = MyClassWPrimaryConstr()
    o1.printMe()

    val o2 = MyClassWPrimaryConstr("...test...")
    o2.printMe()

    var t1 = MyTime("11:57:26")
    println("t1 = " + t1.serialize())

    var t2 = MyTime(13, 15, 52)
    println("t2 = " + t2.serialize())

    var t3: MyTime?
    // ... after 300 linit de cod
    if (t2.hours == 14)
        t3 = null
    else
        t3 = MyTime(t2)

// if (t3 != null) println(t3.serialize())
    println(t3?.serialize())
}