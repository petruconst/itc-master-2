fun main(args: Array<String>) {
    println("Hello world, Kotlin!")
    var a: Int = 12000
    val d: Double = 101.23
    val f: Float = 11.5f

    println("a = " + a)
    a = a + 1
    println("a = " + a)

    println("d = " + d + ", f = " + f)
//    This is not allowed because d is val not var
//    d = d + 1
//    println("d = " + d + ", f = " + f)

    var rs: String = "I am a string"
    println("rs = " + rs)
    rs += "..."
    println("rs = " + rs)

//    Array
//    Even though it's val, the address is constant so elements can be changed
    val numA: IntArray = intArrayOf(1, 2, 3, 4, 5)
    println("numA[2] = " + numA[2])
    numA[2] = 100
    println("numA[2] = " + numA[2])

    val nums: MutableList<Int> = mutableListOf(1, 2, 3)
    val readOnlyView: List<Int> = nums

    println("my mutable list = " + nums)
    nums.add(4)
    println("my mutable list = " + nums)

    println("my immutable list = " + readOnlyView)
    //readOnlyView.drop(2)
    nums.removeAt(2)
    println("my immutable list = " + readOnlyView)

    println("my mutable list = " + nums)
}