import java.util.*
import java.security.*

import javax.crypto.*
import javax.crypto.spec.*

class CryptoJAES {
    fun aesCryptoECB(inputdata: ByteArray, key: ByteArray, mode: Int): ByteArray? {
        try {
            val cipher = Cipher.getInstance("AES/ECB/NoPadding")
            val secretKeySpec = SecretKeySpec(key, "AES")

            if (mode == 0) {
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec)
            } else {
                cipher.init(Cipher.DECRYPT_MODE, secretKeySpec)
            }

            return cipher.doFinal(inputdata)

        } catch(e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun aesCryptoCBC(inputdata: ByteArray, key: ByteArray, iv: ByteArray, mode: Int): ByteArray? {
        try {
            val cipher = Cipher.getInstance("AES/CBC/NoPadding")
            val secretKeySpec = SecretKeySpec(key, "AES")
            val ivSpec = IvParameterSpec(iv)
            if (mode == 0) {
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivSpec)
            } else {
                cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivSpec)
            }

            return cipher.doFinal(inputdata)
        } catch(e: Exception) {
            e.printStackTrace()
        }
        return null
    }

}

fun main(args: Array<String>) {
    val testText = "Hey! Java/Kotlin"
    val password = "password@1234567"

    var c  = CryptoJAES()
    var encTextBytes = c.aesCryptoECB(testText.toByteArray(), password.toByteArray(), 0)
    println("Test enc base64: = \n" +Base64.getEncoder().encodeToString(encTextBytes) )
    var textBytes = c.aesCryptoECB(encTextBytes!!, password.toByteArray(), 1)
    println("Test dec: = \n"  + textBytes!!.toString(Charsets.ISO_8859_1))

    var iv = "1234567812345678"
    var encTextBytesCBC = c.aesCryptoCBC(testText.toByteArray(), password.toByteArray(), iv.toByteArray(), 0)
    println("Test enc base64: = \n" +Base64.getEncoder().encodeToString(encTextBytesCBC) )
    var textBytesCBC = c.aesCryptoCBC(encTextBytesCBC!!, password.toByteArray(), iv.toByteArray(), 1)
    println("Test dec: = \n"  + textBytesCBC!!.toString(Charsets.ISO_8859_1))

}