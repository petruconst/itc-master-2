interface ExInterf {
    var myVar: Int // abstract property
    fun absMethod(): String

    fun hello() {
        println("default method hello in the interface")
    }
}

class InterImpl : ExInterf {
    override var myVar: Int = 25
    override fun absMethod(): String {
        return "Learn Kotlin ...!"
    }
}

class GenericsEx<T>(input: T) {
    init {
        println("genericsEx class sample init input:T = " + input)
    }
}

fun main(args: Array<String>) {
//    var obj = InterImpl()
    var obj: ExInterf
    obj = InterImpl()

    println("obj.myVar = ${obj.myVar}")
    obj.hello()
    println("obj.absMethod = ${obj.absMethod()}")

//    Generics
    var obj1 = GenericsEx<String>("Java/Kotlin")
    var obj2 = GenericsEx<Int>(10)
    
}
