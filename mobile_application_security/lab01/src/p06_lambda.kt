fun main(args: Array<String>) {
    val myL: (String) -> Unit

    myL = { s:String -> println(s)}
    val vstr: String = "Tutorial Kotlin!"

    myL(vstr)
    println(MyFunction("Kotlin"))

    myFun("Kotlin", myL)
}

fun myFun(a: String, action: (String) -> Unit)
{
    println("Hi ")
    action(a)
}

fun MyFunction(x: String): String {
    var r:String = "Hey! Welcome 2 ..."
    return r + x
}