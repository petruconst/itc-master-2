fun main(args: Array<String>) {
    val a1: Int = 5
    val b1: Int = 2
    var max1: Int

    if (a1 > b1) {
        max1 = a1
    } else {
        max1 = b1
    }

    println("max1 " + max1)

    val x: Int = 7
    when (x) {
        1 -> println("x == 1")
        2 -> println("x == 2")
        else -> {
            println("x != 2 && x != 1")
        }
    }

    val items = listOf(7, 11, 23, 3)
    for(i in 0..items.lastIndex) {
        println(items[i])
    }

    for(obj in items) {
        println("item = " + obj)
    }
}