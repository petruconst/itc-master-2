open class Vehicle{
    private var weigth: Float
    var weigthProperty: Float
        get() = this.weigth
        set(value) { this.weigth  = value}

    constructor(weigth: Float = 0.0f) {
        this.weigth = weigth
    }

    open fun printMe() : String {
        var r: String = "Vehicle::weigth = " + this.weigth
        return r
    }
}

class Auto : Vehicle {
    private var doorsNumber: Int
    var doorsNumberProperty: Int
        get() = doorsNumber
        set(value) { this.doorsNumber = value}

    constructor(weigth: Float = 0.0f, doorsNo: Int = 0) : super(weigth) {
        this.doorsNumber = doorsNo
    }

    override fun printMe() : String {
        var r: String = "Auto::weigth = " + this.weigthProperty + ", doorsNo = " + this.doorsNumber
        return r
    }
}

class Plane : Vehicle {
    private var capacity: Float
    var capacityProperty: Float
        get() = this.capacity
        set(value) { this.capacity = value}
    private var enginesNo: Int

    constructor(weigth: Float = 0.0f, capacity: Float = 0.0f, enginesNo: Int = 0)
     : super(weigth) {
        this.capacity = capacity
        this.enginesNo = enginesNo
    }

    override fun printMe() : String {
        var r: String = "Plane::weigth = " + this.weigthProperty + ", capacity = " + this.capacity + ", enginesNo = " + this.enginesNo
        return r
    }
}

fun main(args: Array<String>) {
    var vob1: Vehicle = Vehicle(100.2f)
    var vob2: Vehicle = Vehicle(55.4f)
    println("vob1 = " + vob1.printMe())
    println("vob2 = " + vob2.printMe())

    vob2 = vob1
    vob2.weigthProperty = 300.0f
    println("vob1 = " + vob1.printMe())
    println("vob2 = " + vob2.printMe())

    var v: Vehicle
    var a: Auto = Auto(1200.02f, 3)
    var p: Plane = Plane(10650.2f, 1200.0f, 2)

    v = a
    println("v = " + v.printMe())

    v = p
    println("v = " + v.printMe())

//    v = a
//    p = v as Plane
//    p.printMe()
}