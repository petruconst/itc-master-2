package ro.ism.lab04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private EditText first;
    private EditText second;
    private TextView result;
    private IService mService;
    private ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            mService = IService.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected");
            mService = null;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        first = findViewById(R.id.et_first);
        second = findViewById(R.id.et_second);
        result = findViewById(R.id.tv_result);

        if (mService == null) {
            Intent intent = new Intent(this, RemoteService.class);
            boolean result = getApplicationContext().bindService(intent, mServiceConn, Context.BIND_AUTO_CREATE);
            Log.d(TAG, "service created = " + result);
        }
    }

    public void btnClick(View view) {
        String sFirst = first.getText().toString();
        String sSecond = second.getText().toString();

        try {
            String res = mService.concatenate(sFirst, sSecond);
            result.setText(res);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onDestroy() {
        unbindService(mServiceConn);
        super.onDestroy();
    }
}
