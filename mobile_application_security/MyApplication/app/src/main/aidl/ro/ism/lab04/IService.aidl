// IService.aidl
package ro.ism.lab04;

// Declare any non-default types here with import statements

interface IService {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    String concatenate(String a, String b);
}
