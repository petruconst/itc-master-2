#include <stdio.h>
#include <string.h>

long compute_hash(long hash_seed, const char* password)
{
    for(int i = 0;  i < strlen(password); i++)
    {
        hash_seed = 31 * hash_seed + password[i];
    }
    return hash_seed;
}

int login(long hash_seed, const char* username, const char* password)
{
    char local_buffer[20];
    int ok = 0;
    //! NOTE: may set ok to 1 if length is not checked
    // srtncpy(local_buffer, username, 20);
    strcpy(local_buffer, username);

    if(strcmp(local_buffer, "Admin") == 0)
    {
        //! NOTE: buffer might overflow
        //! strncpy(local_buffer, password, 20);
        strcpy(local_buffer, password);
        //! NOTE: Should cast to long before comparison
        //! (long)-1255088702
        // 9223372036854775807
        if(compute_hash(hash_seed, local_buffer) == -1255088702)
        {
            ok = 1;
        }
    }
    return ok;
}

int main()
{
    char username[20];
    char password[20];

    printf("Please input the username: ");
    //! NOTE: scanf should have a max len
    //! scanf("%19s", username);
    scanf("%s", username);

    printf("Please input the password: ");
    //! NOTE: same as above
    //! scanf("%19s", password);
    scanf("%s", password);

    int hash_seed = 0;
    printf("HASH SEED %d", hash_seed);
    int result = login(hash_seed, username, password);

    if(result)
    {
        printf("Congrats! You have been successfully logged in!");
    }
    else
    {
        printf("Wrong username or password!");
    }
}