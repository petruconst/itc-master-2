package ro.ase.ism.scs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Main2 {

	public static void main(String[] args) {
        Random random = new Random();
        int value = random.nextInt(99_999);

        String fileName = String.format("tempFile%s.bin", value);
        File file = new File(fileName);
        FileWriter writer;
		try {
			writer = new FileWriter(file);
	        writer.write("secret info");
	        writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

        //The file is in use
        try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        //
        
        file.delete();
	}

}
